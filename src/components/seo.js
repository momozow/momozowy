import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

export default function Head({ description, lang, meta, title }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description

  return (
      <>
        <title>{title} | {site.siteMetadata.title}</title>
        <meta name="description" content={metaDescription} />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:creator" content={site.siteMetadata.author} />
        <meta name="twitter:description" content={metaDescription} />
        <meta name="twitter:title" content={title} />
        <meta property="og:description" content={metaDescription} />
        <meta property="og:title" content={title} />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="https://www.momozow.com/favicon.svg" />
      </>
  )
}

Head.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

Head.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}
