import React from "react"
import Contents from "../components/contents"
import MainLayout from "../components/mainlayout"
import Head from "../components/seo.js"

export default () => (
    <MainLayout>
      <Head title="Home" />
      <Contents />
    </MainLayout>
)
